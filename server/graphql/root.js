// GraphQL resolver
var Songs = require('../lib/songs');
var path = require('path');
var library = new Songs(path.join(__dirname, '..', 'data'));
var async = require('async');
var getLibrary = function() {
    return library.getLibrary();
}
// Design and implement GraphQL query and resolver -- Start
var getPlaylist = async function (){
    let promise = new Promise((resolve, reject) => {
        library.getPlaylists(function (err, playlists) {
            resolve(playlists);
        });
      });
    
    let result = await promise;
    return result;
}
var getPlaylistbyId = function(args) {
    var id = parseInt(args.id, 10);
    var data = library.getPlaylist(id);
    var songs =  library.getLibrary();
    let tempvalue = [];
    songs.forEach((value, index)=>{
      var tempId  = data.songs.indexOf(value.id);
      if(tempId != -1) {
        tempvalue.push(value);
      }
    })
    data.songs = tempvalue;
    return data
}
var getLibrarybyId = function(args){
    var id = parseInt(args.id, 10);
    var data = library.getSong(id);
    return data
}
//  Design and implement GraphQL query and resolver -- End
// Design and implement GraphQL mutation query. -- Start
var addPlaylist = async function(args) {
    var name = args.name
    var songs = args.songs
    let promise = new Promise((resolve, reject) => {
        library.savePlaylist(null, name, songs, function (err, id) {
            resolve(id);
        });
    });
    
    let result = await promise;
    return result
}
var deletePlaylist = function (args) {
    var id = parseInt(args.id, 10);
    var data = library.deletePlaylist(id);
    return true
}
var updatePlaylist = async function (args) {
    var id = parseInt(args.id, 10);
    var name = args.name
    var songs = args.songs
    let promise = new Promise((resolve, reject) => {
        library.savePlaylist(id, name, songs, function (err, id) {
            resolve(id)
        });
    });
    let result = await promise;
    return result
}
// Design and implement GraphQL mutation query. -- End
//Search for a particular entry/song criteria (ie: music length, by artist, by genre, etc) from the backend GraphQL API. -- Start
var searchLibrary =  function (args){
    var search = args.search
    var searchData = library.getLibrary();
    var data = searchData.filter(res=>{
        return res.album.includes(search) || res.title.includes(search) || res.artist.includes(search)
    })
    return data;
}
//Search for a particular entry/song criteria (ie: music length, by artist, by genre, etc) from the backend GraphQL API. -- End
var root = { 
    getLibrary : getLibrary,
    getPlaylist :getPlaylist,
    getPlaylistbyId:getPlaylistbyId,
    getLibrarybyId:getLibrarybyId,
    addPlaylist:addPlaylist,
    deletePlaylist:deletePlaylist,
    updatePlaylist:updatePlaylist,
    searchLibrary : searchLibrary,
 };
module.exports  = root