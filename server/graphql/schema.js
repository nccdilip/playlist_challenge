// GraphQL Schema
var { buildSchema } = require('graphql');
// Design and implement GraphQL query and resolver  -- Start
var schema = buildSchema(`
type Query {
    getLibrary: [Library]
    getPlaylist: [Playlists]
    getPlaylistbyId(id:ID!) :Playlist
    getLibrarybyId(id:ID!) :Library
    searchLibrary(search:String!):[Library]
}
type Mutation {
    addPlaylist(name:String, songs:[Int]): ID
    deletePlaylist(id:ID!): Boolean
    updatePlaylist(id:ID!,name:String, songs:[Int]): Boolean
}
type Library {
    album: String
    duration: Int 
    title: String
    id: ID
    artist: String
}
type Playlists {
    id:ID
    name:String
    songs:[Int]
}
type Playlist {
    id:ID
    name:String
    songs:[Library]
}
`);
// Design and implement GraphQL query and resolver -- End 
module.exports = schema