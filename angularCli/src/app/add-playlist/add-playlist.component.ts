import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { DialogData } from '../view-playlist/view-playlist.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CoreAPIService } from '../core-api.service';

@Component({
  selector: 'app-add-playlist',
  templateUrl: './add-playlist.component.html',
  styleUrls: ['./add-playlist.component.scss']
})
export class AddPlaylistComponent implements OnInit {
  name = "";
  addPlaylist: boolean = true;
  @ViewChild('addLibrary') addLibrary;
  constructor(
    public dialogRef: MatDialogRef<AddPlaylistComponent>,
    private coreAPI: CoreAPIService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }
  ngOnInit() {
    this.addPlaylist = true;
  }
  onNoClick(): void {
    this.dialogRef.close({ event: 'close' });
  }
  addToPlaylist() {
    var song: Array<number> = this.addLibrary.selection._selected;
    if (song.length != 0 && this.name != "") {
      var data = {
        name: this.name,
        songs:song
      }
      this.coreAPI.addPlaylist(data).subscribe({
        next: (res) => {
          this.dialogRef.close({ event: 'add' });
        }
      })
    }

  }

}
