import { Injectable } from '@angular/core';
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CoreAPIService {
  getPlaylistQuery = {
    query: gql`
      {
        getPlaylist {
          id
          name
        }
      }
    `,
  };

  constructor(private apollo: Apollo) { }
  
  getLibrary(){
    return this.apollo
    .watchQuery({
      query: gql`
        {
          getLibrary {
            id
            title
            album
            artist
          }
        }
      `,
    })
    .valueChanges;
  }
  getPlaylist(){
    return this.apollo.watchQuery(this.getPlaylistQuery).valueChanges;
  }
  getPlaylistById(id){
    return this.apollo
    .watchQuery({
      query: gql`
      {
        getPlaylistbyId(id:${id}) {
          id
          name
          songs {
            id
            title
            album
            artist
          }
        }
      }
      `,
    })
    .valueChanges;
  }
  addPlaylist(data){
    return this.apollo
    .mutate({
      mutation: gql`
      mutation {
        addPlaylist(name:"${data.name}",songs:${data.songs})
      }
      `,
      refetchQueries:[
        this.getPlaylistQuery
      ]
    });
  }
  deletePlaylist(id) {
    return this.apollo
    .mutate({
      mutation: gql`
      mutation {
        deletePlaylist(id:"${id}")
      }
      `,
      refetchQueries:[
        this.getPlaylistQuery
      ]
    });
  }
  searchLibrary(value){
    return this.apollo
    .watchQuery({
      query: gql`
      {
        searchLibrary(search:"${value}") {
          id
          title
          album
          artist
        }
      }
      `,
    })
    .valueChanges;
  }
}
