import { Component, OnInit, ViewChild , OnDestroy  } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { CoreAPIService } from '../core-api.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AddPlaylistComponent } from '../add-playlist/add-playlist.component';
import { ViewPlaylistComponent } from '../view-playlist/view-playlist.component';
import { DeletePlaylistComponent } from '../delete-playlist/delete-playlist.component';
import { ApolloQueryResult } from 'apollo-client/core/types';
import { Subscription } from 'rxjs';
export interface PlaylistElement {
  id: number,
  name: string,
}
export interface GetPlaylist {
  getPlaylist: Array<PlaylistElement>
}
@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.scss']
})
export class PlaylistComponent implements OnInit, OnDestroy {

  private querySubscription: Subscription;

  displayedColumns: string[] = ['position', 'name', 'action'];
  dataSource = new MatTableDataSource<PlaylistElement>([]);
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private coreAPI: CoreAPIService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getPlaylist();
  }
  getPlaylist() {
    this.querySubscription =  this.coreAPI.getPlaylist().subscribe({
      next: (res: ApolloQueryResult<GetPlaylist>) => {
        this.dataSource = new MatTableDataSource<PlaylistElement>(res.data.getPlaylist);
        this.dataSource.paginator = this.paginator;
      }
    })
  }
  ngOnDestroy () {
    if(this.querySubscription)
      this.querySubscription.unsubscribe();
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  addPlaylist(): void {
    const dialogRef = this.dialog.open(AddPlaylistComponent, {
      width: '90%',
    });

    dialogRef.afterClosed().subscribe(result => {
      // if (result.event == 'add') {
      // //  this.getPlaylist();
      // }
      console.log('The dialog was closed');
      //this.animal = result;
    });
  }
  viewPlaylist(id): void {
    const dialogRef = this.dialog.open(ViewPlaylistComponent, {
      width: '90%',
      data:{ id: id}
    });

    dialogRef.afterClosed().subscribe(result => {
     
      console.log('The dialog was closed');
      //this.animal = result;
    });
  }
  deletePlaylist(id) {
    const dialogRef = this.dialog.open(DeletePlaylistComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event == 'add') {
        this.coreAPI.deletePlaylist(id).subscribe();
      }
      console.log('The dialog was closed');
      //this.animal = result;
    });

  }
}
