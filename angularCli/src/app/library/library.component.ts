import { Component, OnInit, ViewChild, Input, OnDestroy } from '@angular/core';
import { CoreAPIService } from '../core-api.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { ApolloQueryResult } from 'apollo-client/core/types';
import { Subscription } from 'rxjs';

export interface PeriodicElement {
  album: string,
  artist: string,
  duration: number,
  id: number,
  title: string,
}
export interface GetLibrary {
  getLibrary: Array<PeriodicElement>,
}

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.scss']
})
export class LibraryComponent implements OnInit, OnDestroy {

  displayedColumns: string[] = ['position', 'title', 'album', 'artist'];
  dataSource = new MatTableDataSource<PeriodicElement>([]);
  private querySubscription: Subscription;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @Input() addPlaylist: boolean = false;
  @Input() viewPlaylist: boolean = false;
  constructor(
    private coreAPI: CoreAPIService) { }

  ngOnInit(): void {
    if (this.addPlaylist) {
      this.displayedColumns.push('select');
    }
    if(!this.viewPlaylist) {
      this.querySubscription =  this.coreAPI.getLibrary().subscribe({
        next:(res:ApolloQueryResult<GetLibrary>) =>{
          this.getdataSource(res.data.getLibrary);
        }
      })
    }
  }
  ngOnDestroy() {
    if(this.querySubscription)
      this.querySubscription.unsubscribe()
  }
  getdataSource(data){
    this.dataSource = new MatTableDataSource<PeriodicElement>(data);
    this.dataSource.paginator = this.paginator;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  selection = new SelectionModel<number>(true, []);

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row.id));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: PeriodicElement): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row.id) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }
}
