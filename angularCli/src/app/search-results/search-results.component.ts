import { Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { PeriodicElement } from '../library/library.component';
import { Subscription } from 'rxjs';
import { CoreAPIService } from '../core-api.service';
import { ApolloQueryResult } from 'apollo-client';
import { MatPaginator } from '@angular/material/paginator';

export interface SearchLibrary {
  searchLibrary: Array<PeriodicElement>,
}
@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent implements OnInit, OnDestroy {
  search = '';
  displayedColumns: string[] = ['position', 'title', 'album', 'artist'];
  dataSource = new MatTableDataSource<PeriodicElement>([]);
  private querySubscription: Subscription;
  private paramSubscribe: Subscription;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  constructor(private route: ActivatedRoute, private coreAPI: CoreAPIService) { }
  ngOnInit() {
    this.paramSubscribe = this.route.params.subscribe(params=>{
      this.search = params.value;
      if(this.querySubscription){
        this.querySubscription.unsubscribe()
      }
      this.querySubscription =  this.coreAPI.searchLibrary(this.search).subscribe({
        next:(res:ApolloQueryResult<SearchLibrary>) =>{
          this.getdataSource(res.data.searchLibrary);
        }
      })
    })
  }
  getdataSource(data){
    this.dataSource = new MatTableDataSource<PeriodicElement>(data);
    this.dataSource.paginator = this.paginator;
  }
  ngOnDestroy() {
    if(this.querySubscription)
      this.querySubscription.unsubscribe()
    if(this.paramSubscribe)
    this.paramSubscribe.unsubscribe()
  }
}
