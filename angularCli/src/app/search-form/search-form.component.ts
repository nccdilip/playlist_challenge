import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { CoreAPIService } from '../core-api.service';


@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  search = '';
  constructor(private router:Router, private coreAPI: CoreAPIService) { }
  ngOnInit() {

  }
  Submit(){
    if(this.search != ''){
      this.router.navigate([`/search/${this.search}`])
    }
  }
  focusOutFunction(){
    this.search = '';
  }
}
