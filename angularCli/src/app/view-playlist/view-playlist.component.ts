import { Component, OnInit, Inject, ViewChild, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CoreAPIService } from '../core-api.service';
import { PeriodicElement } from '../library/library.component';
import { ApolloQueryResult } from 'apollo-client/core/types';
import { Subscription } from 'rxjs';

export interface DialogData {
  id: number;
  name: string;
  songs: Array<number>;
}
export interface GetPlaylistbyId {
  getPlaylistbyId : DialogData
}

@Component({
  selector: 'app-view-playlist',
  templateUrl: './view-playlist.component.html',
  styleUrls: ['./view-playlist.component.scss']
})
export class ViewPlaylistComponent implements OnInit, OnDestroy {

  name;
  playlistSong: Array<number>;
  @ViewChild('viewlibrary') viewlibrary;
  viewPlaylist: boolean = true;
  private querySubscription: Subscription;
  constructor(
    public dialogRef: MatDialogRef<ViewPlaylistComponent>,
    private coreAPI: CoreAPIService,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }
  ngOnInit() {
    this.viewPlaylist = true;
    this.querySubscription =  this.coreAPI.getPlaylistById(this.data.id).subscribe({
      next: (res : ApolloQueryResult<GetPlaylistbyId>)=>{
        this.name = res.data.getPlaylistbyId.name;
        this.viewlibrary.getdataSource(res.data.getPlaylistbyId.songs);
      }
    })
  }

  ngOnDestroy () {
    if(this.querySubscription)
      this.querySubscription.unsubscribe();
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
